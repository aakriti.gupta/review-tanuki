# frozen_string_literal: true

require 'logger'
require 'gitlab'
require 'pry-byebug'

require_relative 'review_tanuki/merge_request'
require_relative 'review_tanuki/trainee_issue'
require_relative 'review_tanuki/processor'
require_relative 'review_tanuki/synchronizer'

module ReviewTanuki
  class Error < StandardError; end
  # Your code goes here...
end
